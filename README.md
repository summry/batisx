BatisX是Python中类似MyBatis操作数据库的框架，主打的就是简单易用，容易上手，代码简洁，性能极高，因为对数据库驱动只做了一层薄薄的封装，整个安装包只有十几kb，目前支持MySQL和PostgreSQL。PgSqlx提供了多种操作数据库的方式：

- 类似MyBatis接口调用执行SQL，SQL与代码分离，接口可复用。
- 类似iBatis根据sql_id执行SQL，提供了更多操作数据库的函数，例如分页查询等。

- 可以直接执行SQL语句。

- ORM方式操作数据库。



##### 安装

如果网速较慢，也可以指定国内镜像安装

```shell
# pip install batisx
或
# pip install batisx -i https://mirrors.aliyun.com/pypi/simple
```
Mapper文件
你可以在mapper文件夹下创建person_mapper.xml文件，如下：
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "https://gitee.com/summry/batisx/blob/master/dtd/mapper.dtd">
<mapper namespace="person">
   <select id="select_all">
       select id, name, age from person
    </select>

    <select id="select_by_name">
       select id, name, age from person where name = ?
    </select>

    <select id="select_by_name2">
       select id, name, age from person where name = :name
    </select>

    <select id="select_include" include="select_all">
       {{ select_all }}
         {% if name -%}
          where name = :name
         {%- endif -%}
    </select>
</mapper>
```


##### 使用例子

```
from typing import List, Tuple, Mapping
from batisx import mapper, sql, db, dbx, init_db

@mapper(namespace='person')
def select_all(): List

@mapper(namespace='person')
def select_by_name(name: str): List

@mapper(namespace='person')
def select_by_name2(name: str): List

@mapper(namespace='person')
def select_include(name: str): List

@sql('select id, name, age from person where name = ?')
def query_by_name(name: str): List(Mapping)

@sql('select id, name, age from person where name = :name')
def query_by_name2(name: str): List(Mapping)

if __name__ == '__main__':
    # init_db('test.db', driver='sqlite3', show_sql=True, debug=True, mapper_path='./mapper')
    # init_db("postgres://user:password@127.0.0.1:5432/testdb", driver='psycopg2', pool_size=5, mapper_path='./mapper')
    init_db(host='127.0.0.1', port='3306', user='xxx', password='xxx', database='test', pool_size=5, show_sql=True, mapper_path='./mapper')
    
    persons = select_all()
    # result:
    # (3, 'zhangsan', 15)
    # (4, 'lisi', 26)
    # (5, 'wangwu', 38)
    
    persons = select_by_name('zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    persons = select_by_name2(name='zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    persons = select_include(name='zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    persons = query_by_name('zhangsan')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    
    persons = query_by_name2(name='zhangsan')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    
    # 用dbx根据sql_id执行mapper文件里的SQL, sql_id: namespace + id
    persons = dbx.select('person.select_all')  
    # result:
    # (3, 'zhangsan', 15)
    # (4, 'lisi', 26)
    # (5, 'wangwu', 38)

    persons = dbx.query('person.select_all')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    # {'id': 4, 'name': 'lisi', 'age': 26}
    # {'id': 5, 'name': 'wangwu', 'age': 38} 
    
    persons = dbx.select('person.select_by_name', name='zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    # 用db直接执行SQL语句
    effected_rowcount = db.insert(table='person', name='zhaoliu', age=66)
    
    persons = db.select('select id, name, age from person')
    # result:
    # (3, 'zhangsan', 15)
    # (4, 'lisi', 26)
    # (5, 'wangwu', 38)
    # (6, 'zhaoliu', 45)

    persons = db.query('select id, name, age from person name = :name', name='zhangsan')
    # result:
    # [{'id': 3, 'name': 'zhangsan', 'age': 15}]
    
    # where条件用? 占位符，直接传入一一对应的参数就可以了
    persons = db.select('select id, name, age from person where name = ?', 'zhangsan')
    # result:
    # [(3, 'zhangsan', 15)]
```
db和dbx的select方法返回类型是List[tuple], query方法返回类型是List[Mapping]



##### 事务

```
from batisx import trans

# 在方法上声明事务
@trans
def test_transaction():
    insert_func(....)
    update_func(....)

# 在代码块中声明事务
def test_transaction2():
    with trans():
        insert_func(....)
    	update_func(....)
```


如果你想要更舒服的操作MySQL数据库，可以使用相似的框架mysqlx：https://gitee.com/summry/mysqlx

如果你要更舒服的操作PostgreSQL数据库，可以使用相似的框架pgsqlx：https://gitee.com/summry/pgsqlx

