import unittest
from batisx import with_connection, init_db
from config import MYSQL, PGSQL
from db_test import full_test as db_test
from dbx_test import full_test as dbx_test
from orm_test import full_test as orm_test
from mapper_test import full_test as mapper_test
from SqlExec_test import full_test as SqlExec
from dbx_sqlid_test import full_test as sqlid_test


class MyTestCase(unittest.TestCase):

    init_db(**PGSQL)

    @with_connection
    def test_db(self):
        db_test()
        self.assertEqual(True, True)

    @with_connection
    def test_dbx(self):
        dbx_test()
        self.assertEqual(True, True)

    @with_connection
    def test_orm(self):
        orm_test()
        self.assertEqual(True, True)

    @with_connection
    def test_mapper(self):
        mapper_test()
        self.assertEqual(True, True)

    def test_sqlexec(self):
        SqlExec()
        self.assertEqual(True, True)

    def test_sqlidexec(self):
        sqlid_test()
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
