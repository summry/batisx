from config import PGSQL, MYSQL
from test import person_mapper
from batisx import init_db, dbx, db
# from db_test import create_truncate_table

def create_truncate_table(table):
    db.do_execute('truncate table %s' % table)


def full_test():
    create_truncate_table('person')
    #######################################################################################################

    rowcount = db.insert('person', name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    assert rowcount == 1, 'insert'
    assert dbx.sql(person_mapper.person_count).get() == 1, 'insert'

    id2 = dbx.sql('person.batch_insert').save('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
    assert id2 > 0, 'save'
    assert dbx.sql(person_mapper.person_count).get() == 2, 'save'

    dbx.sql(person_mapper.person_update).execute('王五', id2)
    assert dbx.sql(person_mapper.select_name).get(id2) == '王五', 'execute'

    dbx.sql('person.person_update2').execute(name='赵六', id=id2)
    assert dbx.sql('person.named_select').query_one(id=id2)['name'] == '赵六', 'execute'

    print(dbx.sql('person.named_select').select_one(id=id2))

    args = [
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56),
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
    ]
    dbx.sql('person.batch_insert').batch_execute(args)
    persons = dbx.sql('person.select_all').select()
    assert len(persons) == 4, 'batch_execute'
    persons = dbx.sql('person.select_all').query()
    assert len(persons) == 4, 'batch_execute'

    persons = dbx.sql('person.named_select').select(id=id2)
    assert len(persons) == 1, 'select'
    persons = dbx.sql('person.named_select').query(id=id2)
    assert len(persons) == 1, 'query'

    persons = dbx.sql('person.select_name').select(id=id2)
    assert len(persons) == 1, 'select'
    persons = dbx.sql('person.select_name').query(id=id2)
    assert len(persons) == 1, 'query'

    persons = dbx.sql('person.named_select').select(id=id2)
    assert len(persons) == 1, 'select'
    persons = dbx.sql('person.named_select').query(id=id2)
    assert len(persons) == 1, 'query'

    dbx.sql('person.delete').execute(id2)
    assert dbx.sql('person.person_count').get() == 3, 'execute delete'

    args = [
        {'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56},
        {'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56}
    ]
    db.batch_insert('person', args)
    assert dbx.sql('person.person_count').get() == 5, 'batch_insert'

    dbx.sql('person.batch_insert2').batch_execute(args)
    assert dbx.sql('person.person_count').get() == 7, 'batch_execute'

    for u in dbx.sql('person.select_all').page(page_num=2, page_size=3).select():
        print(u)


if __name__ == '__main__':
    init_db(**MYSQL)
    # init_db(**PGSQL)

    full_test()

    for u in dbx.sql('person.select_all').page(page_num=2, page_size=3).select():
        print(u)


    print(dbx.sql('person.batch_insert').save('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56))
    print(dbx.sql('person.batch_insert2').save(**{'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56}))

    for u in dbx.sql('person.select_all').page(page_num=1, page_size=3).select():
        print(u)

    for u in dbx.sql('person.select_all').page(page_num=1, page_size=3).query():
        print(u)

