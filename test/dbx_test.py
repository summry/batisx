from config import PGSQL, MYSQL
from test import person_mapper
from batisx import db, dbx,init_db
# from db_test import create_truncate_table

def create_truncate_table(table):
    print(f"dbx_test db: {hex(id(db))}")
    db.execute('truncate table %s' % table)


def full_test():
    print(db.select('select * from person'))
    create_truncate_table('person')
    #######################################################################################################

    rowcount = db.insert('person', name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    assert rowcount == 1, 'insert'
    assert dbx.get(person_mapper.person_count) == 1, 'insert'

    id2 = dbx.save('person.batch_insert', '张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
    assert id2 > 0, 'save'
    assert dbx.get(person_mapper.person_count) == 2, 'save'

    dbx.execute(person_mapper.person_update, '王五', id2)
    assert dbx.get(person_mapper.select_name, id2) == '王五', 'execute'

    dbx.execute('person.person_update2', name='赵六', id=id2)
    assert dbx.query_one('person.named_select', id=id2)['name'] == '赵六', 'execute'

    print(dbx.select_one('person.named_select', id=id2))

    args = [
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56),
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
    ]
    dbx.batch_execute('person.batch_insert', args)
    persons = dbx.select('person.select_all')
    assert len(persons) == 4, 'batch_execute'
    persons = dbx.query('person.select_all')
    assert len(persons) == 4, 'batch_execute'

    persons = dbx.select('person.named_select', id=id2)
    assert len(persons) == 1, 'select'
    persons = dbx.query('person.named_select', id=id2)
    assert len(persons) == 1, 'query'

    persons = dbx.select('person.select_name', id2)
    assert len(persons) == 1, 'select'
    persons = dbx.query('person.select_name', id2)
    assert len(persons) == 1, 'query'

    persons = dbx.select('person.named_select', id=id2)
    assert len(persons) == 1, 'select'
    persons = dbx.query('person.named_select', id=id2)
    assert len(persons) == 1, 'query'

    dbx.execute('person.delete', id2)
    assert dbx.get('person.person_count') == 3, 'execute delete'

    args = [
        {'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56},
        {'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56}
    ]
    db.batch_insert('person', args)
    assert dbx.get('person.person_count') == 5, 'batch_insert'

    dbx.batch_execute('person.batch_insert2', args)
    assert dbx.get('person.person_count') == 7, 'batch_execute'


if __name__ == '__main__':
    init_db(**MYSQL)
    # init_db(**PGSQL)

    full_test()

    for u in dbx.select_page('person.select_all', page_num=2, page_size=3):
        print(u)

    for u in dbx.query_page('person.select_all', page_num=2, page_size=3):
        print(u)

    print(dbx.save('person.batch_insert', '张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56))
    print(dbx.save('person.batch_insert2', **{'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56}))

