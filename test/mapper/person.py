from batisx import mapper, sql
from typing import Any, List, Tuple, Mapping, Sequence


@mapper(namespace='person')
def person_count(): int


@mapper(batch=True)
def batch_insert(args: Sequence[Tuple[str, int, str, int, float, float, float]]): int


@mapper()
def select_name(id: int): str


@mapper()
def person_update(name: str, id: int): int


@mapper()
def person_update2(id: int, name: str): int


@mapper()
def select_all(): list[Mapping]


@mapper(sql_id='select_all')
def select_all2(): list[tuple]


@sql('select * from person where name = ?')
def select_persons(name: str): list


@mapper(sql_id='batch_insert', return_key=True)
def mapper_save(name, age, birth_date, sex, grade, point, money): int


@mapper(sql_id='batch_insert2', return_key=True)
def mapper_save2(name, age, birth_date, sex, grade, point, money): int


@sql('insert into person(name, age, birth_date, sex, grade, point, money) values(?,?,?,?,?,?,?)',
     return_key=True, select_key="SELECT last_insert_rowid()")
def sql_save(name, age, birth_date, sex, grade, point, money): int

@sql('insert into person(name, age, birth_date, sex, grade, point, money) values(:name,:age,:birth_date,:sex,:grade,:point,:money)',
     return_key=True)
def sql_save2(name, age, birth_date, sex, grade, point, money): int
