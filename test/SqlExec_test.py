from batisx import init_db, db
from config import PGSQL,MYSQL

def full_test():
    # select_key = "SELECT currval('person_id_seq')"
    select_key = "SELECT LAST_INSERT_ID()"
    db.sql('INSERT INTO person(name,age) VALUES(?,?)').execute('lisi', 26)
    print(db.sql('INSERT INTO person(name,age) VALUES(?,?)').save('网吧', 27))
    print(db.sql('INSERT INTO person(name,age) VALUES(?,?)').save_select_key(select_key, '网吧', 27))

    print(db.sql('INSERT INTO person(name,age) VALUES(:name, :age)').save_select_key(select_key, name='zhaoliu', age=45))

    print(db.sql('select * from person').select())
    print(db.sql('select * from person').select_one())

    print(db.sql('select id, name, age from person').query())
    print(db.sql('select id, name, age from person where name = ?').query_one('lisi'))
    print(db.sql('select id, name, age from person where name = :name').query_one(name='lisi'))

    print(db.sql('select count(1) from person').get())

    for u in db.sql('select * from person').page(1, 2).select():
        print(u)

    for u in db.sql('select * from person').page(1, 4).select():
        print(u)

    for u in db.page(1, 4).select('select * from person'):
        print(u)

if __name__ == '__main__':
    init_db(**MYSQL)
    full_test()
