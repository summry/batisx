from test.mapper import person
from config import SQLITE_CONF
from batisx import dbx, transaction, init_db


def full_test():
    args = [
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56),
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
    ]
    person.batch_insert(args=args)

    print(person.select_name(4))

    person.person_update('王五', 5)

    person.person_update2(id=6, name='赵六')

    r = person.select_all()
    for i in r:
        print(i)

    r = person.select_all2()
    for i in r:
        print(i)

    for u in person.select_persons('李四'):
        print(u)

    with transaction():
        person.mapper_save('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
        person.mapper_save2(**{'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56})

    person.sql_save('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)

    person.sql_save2(**{'name': '李四', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56})


if __name__ == '__main__':
    init_db('/Users/summy/project/python/sqlx-exec/test/test.db', **SQLITE_CONF)
    # init_db(**PGSQL)
    full_test()
