from batisx import init_db
from config import MYSQL, PGSQL
from db_test import full_test as db_test
from dbx_test import full_test as dbx_test
from mapper_test import full_test as mapper_test
from threading import Thread


if __name__ == '__main__':
    init_db(**MYSQL)
    Thread(target=db_test()).start()
    Thread(target=dbx_test()).start()
    Thread(target=mapper_test()).start()


